import { ActivityService } from './../activity.service';
import { Activity } from './../activity.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.css']
})
export class ActivityDetailComponent implements OnInit {
  activity$: Observable<Activity>;

  constructor(private activityService: ActivityService, private route: ActivatedRoute) { }

  ngOnInit() {
    const activiyId = Number.parseInt(this.route.snapshot.paramMap.get('id'));
    this.activity$ = this.activityService.getActivity(activiyId);
  }

}
