import { ActivityService } from './../activity.service';
import { Activity } from './../activity.model';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityDetailComponent } from './activity-detail.component';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

describe('ActivityDetailComponent', () => {
  let component: ActivityDetailComponent;
  let fixture: ComponentFixture<ActivityDetailComponent>;
  const mockService = jasmine.createSpyObj<ActivityService>('ActivityService', ['getActivity']);
  const testId = 42;
  let testActivity: Activity;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityDetailComponent],
      providers: [
        { provide: ActivityService, useValue: mockService },
        { provide: ActivatedRoute, useValue: { snapshot: { paramMap: { get: () => testId } } } }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    testActivity= new Activity(testId, 'Test activity detail');
    mockService.getActivity.and.returnValue(of(testActivity))
    fixture = TestBed.createComponent(ActivityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the activity\'s title', () => {
    component.activity$ = of(testActivity);
    fixture.detectChanges();
    let activityDetailElement = fixture.nativeElement;
    expect(activityDetailElement.innerHTML).toContain(testActivity.name);
  });

  it('should display the activity\'s description', () => {
    testActivity.description = "Write some tests cause tests are important.";
    component.activity$ = of(testActivity);
    fixture.detectChanges();
    let activityDetailElement = fixture.nativeElement;
    expect(activityDetailElement.innerHTML).toContain(testActivity.description);
  });

  it('should display the activity with the id specified in the url', () => {
    expect(mockService.getActivity).toHaveBeenCalledWith(testId);
    component.activity$.subscribe(result => {
      expect(result).toEqual(testActivity);
    })
  });
});
