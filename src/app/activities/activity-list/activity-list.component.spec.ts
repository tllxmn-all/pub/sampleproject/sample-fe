import { Activity } from './../activity.model';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityListComponent } from './activity-list.component';
import { ActivityService } from '../activity.service';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { RouterLinkWithHref } from '@angular/router';

describe('ActivityListComponent', () => {
  let component: ActivityListComponent;
  let fixture: ComponentFixture<ActivityListComponent>;
  let activityListElement;
  let activityServiceMock = jasmine.createSpyObj('ActivityService', ['getActivities']);
  let testActivity = new Activity(1, 'Test');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ActivityListComponent
      ],
      providers: [
        { provide: ActivityService, useValue: activityServiceMock }
      ],
      imports: [
        RouterTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    activityServiceMock.getActivities.and.returnValue(of([testActivity]));
    fixture = TestBed.createComponent(ActivityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    activityListElement = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a list', () => {
    let listElement = activityListElement.querySelector('ul');
    expect(listElement).toBeTruthy();
  });

  it('should display activities provided by a service', () => {
    let listItemElement = activityListElement.querySelector('ul>li');
    expect(listItemElement).toBeTruthy();
    expect(listItemElement.innerHTML).toContain('Test');
  });

  it('should link to the activity detail page', () => {
    let listItemLink = fixture.debugElement.query(By.css('ul>li>a'));
    expect(listItemLink).toBeTruthy();
    let routerLink = listItemLink.injector.get(RouterLinkWithHref);
    expect(routerLink['commands']).toEqual([testActivity.id]);
    expect(routerLink.href).toEqual('/' + testActivity.id);
  })
});
