import { ActivityService } from './../activity.service';
import { Component, OnInit } from '@angular/core';
import { Activity } from '../activity.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements OnInit {

  activities: Observable<Activity[]>;

  constructor(private activityService: ActivityService) { }

  ngOnInit() {
    this.activities = this.activityService.getActivities();
  }

}
