import { Activity } from './activity.model';
import { TestBed, inject } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { ActivityService } from './activity.service';
import { HttpEventType, HttpEvent } from '@angular/common/http';

describe('ActivityService', () => {

  let service: ActivityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(ActivityService);
  }
  );

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get activities from backend', inject([HttpTestingController], (httpMock: HttpTestingController) => {
    const mockActivities = [
      new Activity(42, 'Test your service')
    ];

    service.getActivities().subscribe(result => {
      expect(result).toEqual(mockActivities);
    });

    let mockReq = httpMock.expectOne(service.url);
    mockReq.flush(mockActivities);
  }));

  it('should get activity by id', inject([HttpTestingController], (httpMock: HttpTestingController) => {
    const testId = 42;
    const mockActivity = new Activity(testId, 'Test activity retrieval by id');

    let activity$ = service.getActivity(testId);

    expect(activity$).toBeTruthy();
    activity$.subscribe(result => {
      expect(result.id).toEqual(testId);
    });

    let mockReq = httpMock.expectOne(service.url + '/' + testId);
    mockReq.flush(mockActivity);
  }));
});
