import { ActivityListComponent } from './activity-list/activity-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityDetailComponent } from './activity-detail/activity-detail.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [ActivityListComponent, ActivityDetailComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class ActivitiesModule { }
