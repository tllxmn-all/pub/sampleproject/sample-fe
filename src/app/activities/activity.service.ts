import { Injectable } from '@angular/core';
import { Activity } from './activity.model';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  private defaultActivity = new Activity(4711, 'Dummy activity', 'This activity is returned if the backend can\'t be reached');

  readonly url = 'http://localhost:8080/activities';

  constructor(private http: HttpClient) { }

  getActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>(this.url).pipe(catchError(() => of([this.defaultActivity])));
  }

  getActivity(id: number): Observable<Activity> {
    return this.http.get<Activity>(this.url + '/' + id).pipe(catchError(() => of(this.defaultActivity)));
  }
}
