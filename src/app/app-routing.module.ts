import { HomeComponent } from './home/home.component';
import { ActivityDetailComponent } from './activities/activity-detail/activity-detail.component';
import { ActivityListComponent } from './activities/activity-list/activity-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'activities/:id', component: ActivityDetailComponent },
  { path: 'activities', component: ActivityListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
