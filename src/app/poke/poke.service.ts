import { PokeInfo } from './poke-info.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PokeInfoDTO } from './poke-info-dto';

@Injectable({
  providedIn: 'root'
})
export class PokeService {
  readonly url: string = 'https://pokeapi.co/api/v2/pokemon';

  constructor(private http: HttpClient) { }

  getPokemon(id: number): Observable<PokeInfo> {
    return this.http.get(`${this.url}/${id}`).pipe(map((pokeInfoDTO: PokeInfoDTO) => {
      return new PokeInfo(pokeInfoDTO.species.name, pokeInfoDTO.sprites.front_default);
    }));
  }

  getRandomPokemon(): Observable<PokeInfo>{
    return this.getPokemon(this.getRandomPokeId());
  }

  getRandomPokeId() {
    return Math.round(Math.random() * 300);
  }
}
