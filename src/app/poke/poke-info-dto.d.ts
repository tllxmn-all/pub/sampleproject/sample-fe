export interface PokeInfoDTO {
    species: {name: string};
    sprites: {front_default: string}
}