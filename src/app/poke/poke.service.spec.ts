import { PokeInfo } from './poke-info.model';
import { PokeInfoDTO } from './poke-info-dto.d';
import { TestBed, inject } from '@angular/core/testing';

import { PokeService } from './poke.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('PokeService', () => {
  let service: PokeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(PokeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the information for a specific pokemon by its id', inject([HttpTestingController], (httpMock: HttpTestingController) => {
    const testId = 25;
    const pokeInfoDTO: PokeInfoDTO = {
      species: { name: 'Pikachu' },
      sprites: { front_default: 'url.to.pikachu.sprite.png' }
    };

    service.getPokemon(testId).subscribe((result: PokeInfo) => {
      expect(result.name).toBe(pokeInfoDTO.species.name);
      expect(result.spriteUrl).toBe(pokeInfoDTO.sprites.front_default);
    });

    let mockReq = httpMock.expectOne(`${service.url}/${testId}`);
    mockReq.flush(pokeInfoDTO);
  }));

  it('should get a random pokemon', inject([HttpTestingController], (httpMock: HttpTestingController) => {
    const mockRandomNumber = 42;
    spyOn(service, 'getRandomPokeId').and.returnValue(mockRandomNumber);
    const mockRandomPokeInfoDTO: PokeInfoDTO = {
      species: { name: 'RandomPokemon' },
      sprites: { front_default: 'random.pokemon.sprite.png' }
    }

    service.getRandomPokemon().subscribe((result: PokeInfo) => {
      expect(result.name).toEqual(mockRandomPokeInfoDTO.species.name);
      expect(result.spriteUrl).toEqual(mockRandomPokeInfoDTO.sprites.front_default);
    });

    const mockReq = httpMock.expectOne(`${service.url}/${mockRandomNumber}`);
    mockReq.flush(mockRandomPokeInfoDTO);
  }));
});
