export class PokeInfo {
    name: string;
    spriteUrl: string;

    constructor(name: string, spriteUrl: string){
        this.name = name;
        this.spriteUrl = spriteUrl;
    }
}