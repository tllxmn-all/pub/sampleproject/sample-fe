import { PokeService } from './../poke/poke.service';
import { PokeInfo } from './../poke/poke-info.model';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pokeInfo$: Observable<PokeInfo>;

  constructor(private pokeService: PokeService) { }

  ngOnInit() {
    this.pokeInfo$ = this.pokeService.getRandomPokemon();
  }

}
