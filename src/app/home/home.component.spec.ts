import { PokeService } from './../poke/poke.service';
import { PokeInfo } from './../poke/poke-info.model';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterLinkWithHref } from '@angular/router';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  const mockService = jasmine.createSpyObj<PokeService>('PokeService', ['getRandomPokemon']);
  const pokeInfoFromMockService = new PokeInfo('Pikachu', 'test.pokemon.url.png');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [HomeComponent],
      providers: [
        { provide: PokeService, useValue: mockService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    mockService.getRandomPokemon.and.returnValue(of(pokeInfoFromMockService));
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should greet the user with a pokemon', () => {
    const testPokemonName = 'TestPokemon';
    component.pokeInfo$ = of(new PokeInfo(testPokemonName, 'test.sprite.url'));
    fixture.detectChanges();
    let homeElement = fixture.nativeElement;
    expect(homeElement.innerHTML).toContain(testPokemonName + ' says \'Hi!\'');
  });

  it('should link to the activities list', () => {
    let linkElement = fixture.debugElement.query(By.css('a'));
    let routerLink = linkElement.injector.get(RouterLinkWithHref);
    expect(routerLink['commands']).toEqual(['activities']);
    expect(routerLink.href).toEqual('/activities');
  });

  it('should display a pokemon', () => {
    const testUrl = 'some.pokemon.url.png';
    component.pokeInfo$ = of(new PokeInfo('TestPokemon', testUrl));
    fixture.detectChanges();
    let imageElement = fixture.nativeElement.querySelector('img');
    expect(imageElement).toBeTruthy();
    expect(imageElement.src.endsWith(testUrl));
  });

  it('should get pokemon info from a service', () => {
    component.pokeInfo$.subscribe((result: PokeInfo) => {
      expect(result).toEqual(pokeInfoFromMockService);
    });
  });
});
